const assets = [
    "index.html",
    "404.html",
    "blank.html",
    "cards.html",
    "forgot-password.html",
    "login.html",
    "register.html",
    "tables.html",
    "utilities-animation.html",
    "utilities-border.html",
    "utilities-color.html",
    "utilities-other.html",
    "img/undraw_posting_photo.svg",
    "img/undraw_profile.svg",
    "img/undraw_profile_1.svg",
    "img/undraw_profile_2.svg",
    "img/undraw_profile_3.svg",
    "img/undraw_rocket.svg",
    "css/sb-admin-2.css",
    "css/sb-admin-2.min.css",
    "js/sb-admin-2.js",
    "js/sb-admin-2.min.js"

]

const statDevPWA = "br.edu.cest.av1.sb-admin2"

self.addEventListener("install", (installEvent)  => {
    installEvent.waitUntil(
        caches.open(statDevPWA).then(
            (cache) => cache.addAll(assets)));
 }
);

self.addEventListener("fetch", (fetchEvent)  => {
    console.log('fetch url capturada:', fetchEvent.request.url);
    fetchEvent.respondWith(
        caches.match(fetchEvent.request).then(
            (cachedResponse) => {
                if (cachedResponse) {
                    return cachedResponse;
                }
                return fetch(fetchEvent.request);

        }),
    );
});
